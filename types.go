package cryptos

import "time"

// CoinMarketCapQuotes
// Docs: https://coinmarketcap.com/api/documentation/v1/#operation/getV1CryptocurrencyQuotesLatest
type CoinMarketCapQuotes struct {
	Data   map[string]CryptoQuote `json:"data"`
	Status struct {
		Timestamp    time.Time `json:"timestamp"`
		ErrorCode    int       `json:"error_code"`
		ErrorMessage string    `json:"error_message"`
		Elapsed      int       `json:"elapsed"`
		CreditCount  int       `json:"credit_count"`
	} `json:"status"`
}

// CryptoQuote is the quote for one crypto
type CryptoQuote struct {
	ID                int                           `json:"id"`
	Name              string                        `json:"name"`
	Symbol            string                        `json:"symbol"`
	Slug              string                        `json:"slug"`
	IsActive          int                           `json:"is_active"`
	IsFiat            int                           `json:"is_fiat"`
	CirculatingSupply int                           `json:"circulating_supply"`
	TotalSupply       int                           `json:"total_supply"`
	MaxSupply         int                           `json:"max_supply"`
	DateAdded         time.Time                     `json:"date_added"`
	NumMarketPairs    int                           `json:"num_market_pairs"`
	CMCRank           int                           `json:"cmc_rank"`
	LastUpdated       time.Time                     `json:"last_updated"`
	Tags              []string                      `json:"tags"`
	Quote             map[string]CryptoQuoteDetails `json:"quote"`
}

// CryptoQuoteDetails details for a single quote
type CryptoQuoteDetails struct {
	Price            float32   `json:"price"`
	Volume24H        float32   `json:"volume_24h"`
	PercentChange1H  float32   `json:"percent_change_1h"`
	PercentChange24H float32   `json:"percent_change_24h"`
	PercentChange7D  float32   `json:"percent_change_7d"`
	PercentChange30D float32   `json:"percent_change_30d"`
	MarketCap        float32   `json:"market_cap"`
	LastUpdated      time.Time `json:"last_updated"`
}

# cryptos

This is because Chris asked for it like he didn't think I'd actually do it. Well I sure showed him.

This is a library for querying the price of a coin using a datasource of your choosing. Currently supported datasources are CoinMarketCap.

## Usage

Create a client, showing three clients just to illustrate the options for setting up different combinations of datasources, you can create a client that can call all datasources, or one, or none:

Use the service for the datasource(s) chosen:

```golang
quotes, err := client.CoinMarketCap.GetQuotes([]string{"XCH"})
```

The `GetQuotes` function in either service simply takes a slice of symbols to query in the datasource. The types returned differ from datasource to datasource based on the structure of data provided. To see the different return types, look at `types.go`.

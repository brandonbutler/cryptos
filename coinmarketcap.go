package cryptos

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

const (
	cmcBaseURL    = "https://pro-api.coinmarketcap.com"
	cmcAuthHeader = "X-CMC_PRO_API_KEY"
)

type CoinMarketCapService struct {
	client *Client
}

// GetQuotes accepts a list of symbols and returns a CoinMarketCapQuotes struct
func (s *CoinMarketCapService) GetQuotes(symbols []string) (CoinMarketCapQuotes, error) {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/v2/cryptocurrency/quotes/latest", cmcBaseURL), nil)
	if err != nil {
		return CoinMarketCapQuotes{}, err
	}
	req.Header.Set("Accepts", "application/json")
	req.Header.Add(cmcAuthHeader, s.client.coinMarketCapToken)

	q := url.Values{}
	q.Add("symbol", strings.Join(symbols, ","))
	req.URL.RawQuery = q.Encode()

	resp, err := s.client.client.Do(req)
	if err != nil {
		return CoinMarketCapQuotes{}, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return CoinMarketCapQuotes{}, err
	}

	var quotes CoinMarketCapQuotes
	if err := json.Unmarshal(body, &quotes); err != nil {
		return CoinMarketCapQuotes{}, err
	}

	return quotes, nil
}

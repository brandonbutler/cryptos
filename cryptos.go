package cryptos

import (
	"net/http"
)

type Client struct {
	client             *http.Client
	coinMarketCapToken string

	// Services
	CoinMarketCap *CoinMarketCapService
}

type ClientOptionFunc func(*Client) error

func NewClient(options ...ClientOptionFunc) (*Client, error) {
	c := &Client{}
	c.client = &http.Client{}

	// Apply any given client options.
	for _, fn := range options {
		if fn == nil {
			continue
		}
		if err := fn(c); err != nil {
			return nil, err
		}
	}

	// Datasources/Services
	c.CoinMarketCap = &CoinMarketCapService{client: c}

	return c, nil
}

func WithCoinMarketCapToken(token string) ClientOptionFunc {
	return func(c *Client) error {
		c.coinMarketCapToken = token
		return nil
	}
}
